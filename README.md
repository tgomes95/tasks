# Tasks #

Simple tasks app

Check out the online version at [tasksx.herokuapp.com](https://tasksx.herokuapp.com/)


## Run  ##


### Virtualenv ###

1. Clone the repository
2. Run the following commands

```
#!shell

cd tasks/
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
cd app
./run
```

Check out [localhost:8000](localhost:8000)


### Docker ###

1. Clone the repository
2. Run the following commands

```
#!shell

cd tasks
git checkout dockerized
docker-compose build
docker-compose up -d
docker-compose run web /usr/local/bin/python create_db.py
```

Check out [localhost:8000](localhost:8000)
