#!/usr/bin/env python
from datetime import datetime

from flask import Flask
from flask import redirect
from flask import request
from flask import render_template
from flask_sqlalchemy import SQLAlchemy

from flask_migrate import Migrate

import config


app = Flask(__name__)
app.config.from_object(config.Development)

db = SQLAlchemy(app)
migrate = Migrate(app, db)


from models.task import Task


@app.route('/')
@app.route('/home')
def tasks_list():
    tasks = Task.query.all()
    return render_template('home.html', tasks=tasks)


@app.route('/add', methods=['POST'])
def add_task():
    description = request.form['description']

    if not description:
        return 'Error'

    task = Task(description)
    db.session.add(task)
    db.session.commit()

    return redirect('/')


@app.route('/delete/<int:task_id>')
def delete_task(task_id):
    task = Task.query.get(task_id)

    if not task:
        return redirect('/')

    db.session.delete(task)
    db.session.commit()

    return redirect('/')


@app.route('/update/<int:task_id>')
def update_task(task_id):
    task = Task.query.get(task_id)

    if not task:
        return redirect('/')

    if task.done:
        task.done = False
        task.finished_at = None
    else:
        task.done = True
        task.finished_at = datetime.utcnow()

    db.session.commit()

    return redirect('/')


@app.errorhandler(403)
def forbidden(error):
    return render_template('403.html'), 403


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


@app.errorhandler(405)
def method_not_allowed(error):
    return render_template('405.html'), 405


@app.errorhandler(500)
def internal_server_error(error):
    return render_template('500.html'), 500


if __name__ == '__main__':
    app.run(host=config.Development.HOST, port=config.Development.PORT)
