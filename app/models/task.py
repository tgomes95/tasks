from app import db

from datetime import datetime


class Task(db.Model):

    __tablename__ = 'tasks'

    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    finished_at = db.Column(db.DateTime, default=None)
    description = db.Column(db.Text)
    done = db.Column(db.Boolean, default=False)

    def __init__(self, description):
        self.created_at = datetime.utcnow()
        self.description = description
        self.done = False

    def __repr__(self):
        return '<Description %s>' % self.description
